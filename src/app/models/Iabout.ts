export interface About {
    name: string;
    age: number;
    work: string;
    city: string;
}
