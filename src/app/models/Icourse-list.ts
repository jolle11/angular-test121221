export interface Course {
    courseImage: string;
    id: number;
    name: string;
}
