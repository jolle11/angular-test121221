import { Component, OnInit } from '@angular/core';
import { Course } from 'src/app/models/Icourse-list';

@Component({
    selector: 'app-course-list',
    templateUrl: './course-list.component.html',
    styleUrls: ['./course-list.component.scss'],
})
export class CourseListComponent implements OnInit {
    courseList?: Course[];
    constructor() {}

    ngOnInit(): void {
        console.log('You are now inside Course List');
        this.courseList = [
            {
                courseImage: 'https://tecnoticias.net/wp-content/uploads/2021/02/mongodb-atlas-google-cloud-partnership-nosql-databases-integrations-2.jpg',
                id: 1,
                name: 'Learning MongoDB',
            },
            {
                courseImage:
                    'https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lgyno4NC7rhy49BAEjN%2F-Lh14lb3LH4C886qWxYA%2F-Lh1DZeIUQennGd9RiHe%2FScreen%20Shot%202019-06-10%20at%2011.30.20%20AM.png?alt=media&token=784b79f6-81b5-4308-97a2-155afb9d496f',
                id: 2,
                name: 'Learning ExpressJS',
            },
            {
                courseImage:
                    'https://res.cloudinary.com/practicaldev/image/fetch/s--DYfpZirq--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://miro.medium.com/max/990/1%2AOc2PsJ-QKOUG2I8J3HNmWQ.png',
                id: 3,
                name: 'Learning Angular',
            },
            {
                courseImage: 'http://xurxodev.com/content/images/2015/12/Node-js-Logo.png',
                id: 4,
                name: 'Learning NodeJS',
            },
        ];
    }
}
