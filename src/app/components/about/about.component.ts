import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
    name: string = 'John';
    age: number = 30;
    work: string = 'Udemy';
    city: string = 'San Francisco';
    constructor() {}

    ngOnInit(): void {
        console.log('You are now inside About');
    }
}
