import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
    imgUdemy: string = 'https://upload.wikimedia.org/wikipedia/commons/e/e3/Udemy_logo.svg';
    constructor() {}

    ngOnInit(): void {}
}
